package com.asmtunisie.teenjobhub.authentication_service.controllers;

import com.asmtunisie.teenjobhub.authentication_service.repositories.RoleRepository;
import com.asmtunisie.teenjobhub.authentication_service.repositories.UserRepository;
import com.asmtunisie.teenjobhub.authentication_service.services.UserDetailsImpl;
import com.asmtunisie.teenjobhub.authentication_service.utils.TokenProvider;
import com.asmtunisie.teenjobhub.models.payload.request.LoginRequest;
import com.asmtunisie.teenjobhub.models.payload.request.SignupRequest;
import com.asmtunisie.teenjobhub.models.payload.response.MessageResponse;
import com.asmtunisie.teenjobhub.models.payload.response.UserInfoResponse;
import com.asmtunisie.teenjobhub.models.usermodels.ERole;
import com.asmtunisie.teenjobhub.models.usermodels.Role;
import com.asmtunisie.teenjobhub.models.usermodels.User;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    final AuthenticationManager authenticationManager;

    final RoleRepository roleRepository;
    final UserRepository userRepository;

    final PasswordEncoder encoder;

    final TokenProvider tokenProvider;

    public AuthController(AuthenticationManager authenticationManager, RoleRepository roleRepository, UserRepository userRepository, PasswordEncoder encoder, TokenProvider tokenProvider) {
        this.authenticationManager = authenticationManager;
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.encoder = encoder;
        this.tokenProvider = tokenProvider;
    }

    @PostMapping("/signup")
    public ResponseEntity<?> signup(@RequestBody SignupRequest signUpRequest) {
        try {
            if (userRepository.existsByUsername(signUpRequest.getUsername())) {
                return ResponseEntity
                        .badRequest()
                        .body(new MessageResponse("Error: Username is already taken!"));
            }

            if (userRepository.existsByEmail(signUpRequest.getEmail())) {
                return ResponseEntity
                        .badRequest()
                        .body(new MessageResponse("Error: Email is already in use!"));
            }
            System.out.println(signUpRequest.getPassword());
            // Create new user's account
            User user = new User(signUpRequest.getUsername(),
                    signUpRequest.getEmail(),
                    encoder.encode(signUpRequest.getPassword()));
            Set<String> strRoles = signUpRequest.getRoles();
            Set<Role> roles = new HashSet<>();

            if (strRoles == null) {
                Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                roles.add(userRole);
            } else {
                strRoles.forEach(role -> {
                    switch (role) {
                        case "admin":
                            Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(adminRole);

                            break;
                        case "mod":
                            Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(modRole);

                            break;
                        default:
                            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(userRole);
                    }
                });
            }
            user.setRoles(roles);
            userRepository.save(user);
            return ResponseEntity.ok(new MessageResponse("User registered successfully!"));

        } catch (HttpClientErrorException.Unauthorized e) {
            return ResponseEntity.ok(new MessageResponse(e.getMessage()));

        }

    }


    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        String jwtToken = tokenProvider.createToken(authentication);

        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, jwtToken)
                .body(new UserInfoResponse(userDetails.getId(),
                        userDetails.getUsername(),
                        userDetails.getEmail(), roles, jwtToken));
    }

    @PostMapping("createRole")
    public ResponseEntity<String> addRole(@RequestParam String roleName) {
        Role role = new Role();
        switch (roleName) {
            case "admin":
               role = new Role(ERole.ROLE_ADMIN);

                break;
            case "mod":
                role = new Role(ERole.ROLE_MODERATOR);


                break;
            default:
                role = new Role(ERole.ROLE_USER);

        }

        try {
            // Check if the role already exists
            if (roleRepository.findByName(role.getName()).isPresent()) {
                return new ResponseEntity<>("Role already exists", HttpStatus.BAD_REQUEST);
            }

            // Create a new role
            Role newRole = new Role(role.getName());
            roleRepository.save(newRole);

            return new ResponseEntity<>("Role added successfully", HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>("Failed to add role", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

