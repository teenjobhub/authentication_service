package com.asmtunisie.teenjobhub.authentication_service.repositories;

import java.util.Optional;

import com.asmtunisie.teenjobhub.models.usermodels.ERole;
import com.asmtunisie.teenjobhub.models.usermodels.Role;
import org.springframework.data.mongodb.repository.MongoRepository;



public interface RoleRepository extends MongoRepository<Role, String> {
    Optional<Role> findByName(ERole name);
}
