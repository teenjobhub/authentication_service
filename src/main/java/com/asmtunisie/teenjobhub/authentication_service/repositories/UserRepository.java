package com.asmtunisie.teenjobhub.authentication_service.repositories;

import com.asmtunisie.teenjobhub.models.usermodels.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String> {
    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}